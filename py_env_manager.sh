#!/bin/sh
#
#    Copyright 2022 Ibai Roman
#
#    This file is part of Python venv Manager.
#
#    Python venv Manager is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Python venv Manager is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Python venv Manager. If not, see <http://www.gnu.org/licenses/>.

[ "$_DEBUG" = "on" ] && set -x

error() {
    printf "\033[1;31merror: %s\033[0m\n" "$1" >&2
}

check_installed() {
    COMMAND=$1

    if ! [ -x "$(command -v "$COMMAND")" ]; then
        error "$COMMAND is not installed."
        exit 1
    fi
}

activate_environment() {
    . $(find "$1" -name 'activate')
}

download_packages() {
    if [ ! -f "$1/requirements.txt" ]; then
        error "requirements.txt not found"
        return
    fi
    echo ">>>>>>> DOWNLOADING PACKAGES <<<<<<<<<"
    echo "creating $1/packages"
    rm -rf "$1/packages"
    mkdir "$1/packages"
    python3 -m venv "$1/.downloadenv" || exit 1
    activate_environment "$1/.downloadenv"
    python -m pip install --upgrade pip
    python -m pip download pip -d "$1/packages" -f "$1/packages"
    python -m pip download -r "$1/requirements.txt" \
        -d "$1/packages" -f "$1/packages"
    deactivate
    rm -rf "$1/.downloadenv"
}

unpack_environment() {
    if [ ! -f "$1/requirements.txt" ]; then
        error "requirements.txt not found"
        return
    fi
    if [ ! -d "$1/packages" ]; then
        error "packages/ not found"
        return
    fi
    echo ">>>>>>> UNPACKING ENVIRONMENT <<<<<<<<<"
    rm -rf "$1/.env"
    python3 -m venv "$1/.env" || exit 1
    activate_environment "$1/.env"
    python -m pip install --upgrade pip --no-index -f "$1/packages"
    python -m pip install -r "$1/requirements.txt" --no-cache-dir \
        --no-index -f "$1/packages"
    deactivate
}

create_environment() {
    echo ">>>>>>> CREATING ENVIRONMENT <<<<<<<<<"
    rm -rf "$1/.env"
    python3 -m venv "$1/.env" || exit 1
    activate_environment "$1/.env"
    python -m pip install --upgrade pip
    if [ -f "$1/pyproject.toml" ]; then
        python -m pip install -e "$1"
    elif [ -f "$1/requirements.txt" ]; then
        python -m pip install -r "$1/requirements.txt"
    fi
    deactivate
}

show_help() {
    cat <<-EOT
	Usage: $SCRIPTNAME <command> [<projectDir>]

	commands:
	    activate    Activate a virtual environment. . $SCRIPTNAME activate.
	    download    Download packages.
	    unpack      Create a virtual environment with downloaded packages.
	    create      Create a virtual environment.
	EOT
}

SCRIPTNAME=${0##*/}

check_installed "python3"

if [ $# -lt 1 ]; then
    show_help
    exit 0
fi

ACTION=$1
shift

if [ $# -lt 1 ]; then
    PROJECTDIR=$(pwd)
else
    PROJECTDIR=$1
fi

case $ACTION in
    activate|ac) activate_environment "$PROJECTDIR";;
    download|dl) download_packages "$PROJECTDIR";;
    unpack|up) unpack_environment "$PROJECTDIR";;
    create|cr) create_environment "$PROJECTDIR";;
    *) show_help
    ;;
esac
